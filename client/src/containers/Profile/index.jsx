import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Button,
  Form
} from 'semantic-ui-react';
import { setUserStatus, updateUserProfile } from './actions';
import UpdateUserStatus from '../../components/Modals/UpdateUserStatus';

const Profile = ({
  user,
  setUserStatus: updateUserStatus,
  updateUserProfile: updateUser
}) => {
  const [statusModal, setStatusModal] = useState(false);
  const [userName, setUserName] = useState(user.username);
  const handleUserProfile = () => {
    updateUser(user.id, userName);
  };
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Form onSubmit={handleUserProfile}>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={userName}
            onChange={ev => setUserName(ev.target.value)}
          />
          <Button type="submit" color="green">Submit</Button>
        </Form>
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Button onClick={() => setStatusModal(true)}>Set status</Button>
      </Grid.Column>
      <UpdateUserStatus
        setUserStatus={updateUserStatus}
        open={statusModal}
        setOpen={setStatusModal}
        userId={user.id}
        userStatus={user.status}
      />
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  setUserStatus: PropTypes.func.isRequired,
  updateUserProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  setUserStatus,
  updateUserProfile
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
