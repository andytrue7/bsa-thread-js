import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const DeletePostModal = ({ open, setOpen, deletePost, id }) => {
  const handleDeleteModal = () => {
    deletePost(id);
    setOpen(false);
  };
  return (
    <Modal
      open={open}
      onClose={() => setOpen(false)}
      closeIcon
    >
      <Modal.Header>Delete post</Modal.Header>
      <Modal.Content>
        Are you sure?
      </Modal.Content>
      <Modal.Actions>
        <Button color="black" onClick={() => setOpen(false)}>
          Nope
        </Button>
        <Button color="red" onClick={handleDeleteModal}>
          Delete
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

DeletePostModal.propTypes = {
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  setOpen: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default DeletePostModal;
