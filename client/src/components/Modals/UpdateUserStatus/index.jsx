import React, { useState } from 'react';
import { Form, Modal, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const UpdateUserStatus = ({ open, setOpen, setUserStatus, userStatus, userId }) => {
  const [status, setStatus] = useState(userStatus);
  const handleUserStatus = async () => {
    setUserStatus(userId, status);
    setOpen(false);
  };

  return (
    <Modal
      open={open}
      onClose={() => setOpen(false)}
      closeIcon
    >
      <Modal.Header>Edit status</Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUserStatus}>
          <Form.TextArea
            name="status"
            value={status}
            onChange={ev => setStatus(ev.target.value)}
          />
          <Button type="submit" color="green">Submit</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdateUserStatus.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  setUserStatus: PropTypes.func.isRequired,
  userStatus: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired
};

export default UpdateUserStatus;
