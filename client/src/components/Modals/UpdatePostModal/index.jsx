import React, { useState } from 'react';
import { Form, Modal, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const UpdatePostModal = ({ open, setOpen, id, updatePost, postBody }) => {
  const [body, setBody] = useState(postBody);
  const handleUpdatePost = async () => {
    await updatePost(id, body);
    setOpen(false);
  };

  return (
    <Modal
      open={open}
      onClose={() => setOpen(false)}
      closeIcon
    >
      <Modal.Header>Edit post</Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdatePost}>
          <Form.TextArea
            name="body"
            value={body}
            onChange={ev => setBody(ev.target.value)}
          />
          <Button type="submit" color="green">Submit</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatePostModal.propTypes = {
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired,
  setOpen: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

export default UpdatePostModal;
