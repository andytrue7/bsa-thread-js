import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Button } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import UpdatePostModal from '../Modals/UpdatePostModal';
import DeletePostModal from '../Modals/DeletePostModal';

const Post = ({ post, userId, likePost, toggleExpandedPost, sharePost, updatePost, deletePost, dislikePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const [updatePostModal, setUpdatePostModal] = useState(false);
  const [deletePostModal, setDeletePostModal] = useState(false);
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {
          userId === user.id
          && <Button color="yellow" onClick={() => setUpdatePostModal(true)}>Edit</Button>
        }
        {
          userId === user.id
          && <Button color="red" onClick={() => setDeletePostModal(true)}>Delete</Button>
        }
      </Card.Content>
      <UpdatePostModal
        open={updatePostModal}
        setOpen={setUpdatePostModal}
        id={id}
        updatePost={updatePost}
        postBody={body}
      />
      <DeletePostModal
        open={deletePostModal}
        setOpen={setDeletePostModal}
        deletePost={deletePost}
        id={id}
      />
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string.isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired
};

export default Post;
