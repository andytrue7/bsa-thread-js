import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const setUserStatus = async (id, status) => {
  const response = await callWebApi({
    endpoint: `/api/auth/${id}/status`,
    type: 'PUT',
    request: {
      status
    }
  });
  return response.json();
};

export const updateUserProfile = async (id, username) => {
  const response = await callWebApi({
    endpoint: `/api/auth/${id}`,
    type: 'PUT',
    request: {
      username
    }
  });
  return response.json();
};
