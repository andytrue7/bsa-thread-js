module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'users',
    'status',
    {
      type: Sequelize.STRING,
      allowNull: true
    }
  ),
  down: queryInterface => queryInterface.removeColumn('users', 'status')
};
