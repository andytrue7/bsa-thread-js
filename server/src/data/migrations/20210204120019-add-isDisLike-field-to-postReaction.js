module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'postReactions',
    'isDisLike',
    {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: true
    }
  ),

  down: queryInterface => queryInterface.removeColumn('postReactions', 'isDisLike')
};
