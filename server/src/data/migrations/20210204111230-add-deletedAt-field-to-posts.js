module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn(
    'posts',
    'deletedAt',
    {
      type: Sequelize.DATE
    }
  ),

  down: queryInterface => queryInterface.removeColumn('posts', 'deletedAt')
};
