import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true, isDisLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => ((react.isLike === isLike || react.isDisLike === isDisLike)
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike, isDisLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike, isDisLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const update = (id, postBody) => postRepository.updateById(id, postBody);

export const deletePost = id => postRepository.deleteById(id);
